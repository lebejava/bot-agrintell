var express = require('express')
var app = express()
var http = require('http').Server(app)
var parser = require('body-parser')
var moment = require('moment')
var fs = require('fs')
var request = require('request')
var cheerio = require('cheerio')
var config = require('./config.js')

app.set('views', __dirname + '/View')
app.set('view engine', 'ejs')
app.set('key', config.secret)
app.use(parser())
app.use(express.static(__dirname + '/Public'))

//APP
app.get('/', function(req, res) {
  res.render('index')
})

//BOT
var url = ''
var intervalSensores = 5000, intervalValvulas = 5000
var sensores = false, valvulas = false
var controladorIds = ["gat123", "gat456", "gat789"]
var controlador1 = [false, false, false],
    controlador2 = [false, false, false],
    controlador3 = [false, false, false]

var sensores1 = ["sensor345", "sensor456", "sensor567"],
    sensores2 = ["sensor678", "sensor789", "sensor890"],
    valvulas1 = ["valvula345", "valvula456", "valvula567"]

function enviarSensores() {
  if(sensores && url) {

    var options = {
      url: url,
      method: 'POST'
    };

    controlador1.forEach(function(enabled, index) {
      if(enabled) {
        options.json = {
          "coordinate": {
          "coordinateid": controladorIds[0],
          "code": 3000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1,
          "date": moment().format("YYYY-MM-DD hh:mm:ss.SSS") + "z"
          },
          "device": {
          "devid": sensores1[index],
          "code": 1000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1,
          "tensiometer1": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer2": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer3": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer4": parseInt(Math.random() * (2000 - 0) + 0),
          "conductivity": parseInt(Math.random() * (100000000 - 0) + 0),
          "temperature": parseInt(Math.random() * (2000 - (-400)) + (-400)),
          "volume_content": parseInt(Math.random() * (1000 - 0) + 0),
          "temperature_station": parseInt(Math.random() * (600 - (-400)) + (-400)),
          "humidity": parseInt(Math.random() * (1000 - 0) + 0),
          "rain": parseInt(Math.random() * (9999 - 0) + 0),
          "speed_wind": parseInt(Math.random() * (1000 - 0) + 0),
          "light": parseInt(Math.random() * (300 - 0) + 0),
          "pressure_air": parseInt(Math.random() * (1100 - 300) + 300),
          "battery2": parseInt(Math.random() * (80 - 0) + 0)
          }
        }
        request(options, function(error, response, body) {
          if(error) {
            console.log('Error:', error)
          }else{
            console.log('SEND OK');
          }
        })
      }
    });

    controlador2.forEach(function(enabled, index) {
      if(enabled) {
        options.json = {
          "coordinate": {
          "coordinateid": controladorIds[1],
          "code": 3000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1,
          "date": moment().format("YYYY-MM-DD hh:mm:ss.SSS") + "z"
          },
          "device": {
          "devid": sensores2[index],
          "code": 1000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1,
          "tensiometer1": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer2": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer3": parseInt(Math.random() * (2000 - 0) + 0),
          "tensiometer4": parseInt(Math.random() * (2000 - 0) + 0),
          "conductivity": parseInt(Math.random() * (100000000 - 0) + 0),
          "temperature": parseInt(Math.random() * (2000 - (-400)) + (-400)),
          "volume_content": parseInt(Math.random() * (1000 - 0) + 0),
          "temperature_station": parseInt(Math.random() * (600 - (-400)) + (-400)),
          "humidity": parseInt(Math.random() * (1000 - 0) + 0),
          "rain": parseInt(Math.random() * (9999 - 0) + 0),
          "speed_wind": parseInt(Math.random() * (1000 - 0) + 0),
          "light": parseInt(Math.random() * (300 - 0) + 0),
          "pressure_air": parseInt(Math.random() * (1100 - 300) + 300),
          "battery2": parseInt(Math.random() * (80 - 0) + 0)
          }
        }
        request(options, function(error, response, body) {
          if(error) {
            console.log('Error:', error)
          }else{
            console.log('SEND OK');
          }
        })
      }
    });

  }
}

function enviarValvulas() {
  if(valvulas && url) {
    var options = {
      url: url,
      method: 'POST'
    };

    controlador3.forEach(function(enabled, index) {
      if(enabled) {
        options.json = {
          "coordinate": {
          "coordinateid": controladorIds[2],
          "code": 3000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1,
          "date": moment().format("YYYY-MM-DD hh:mm:ss.SSS") + "z"
          },
          "device": {
          "devid": valvulas1[index],
          "code": 2000,
          "rssitx": parseInt(Math.random() * (50 - 0) + 0),
          "rssirx": parseInt(Math.random() * (50 - 0) + 0),
          "battery": parseInt(Math.random() * (80 - 0) + 0),
          "latitude": 12000,
          "longitude": 12000,
          "state": 1
          }
        }
        request(options, function(error, response, body) {
          if(error) {
            console.log('Error:', error)
          }else{
            console.log('SEND OK');
          }
        })
      }
    });
  }
}

var botSensores = setInterval(enviarSensores, intervalSensores);

var botValvulas = setInterval(enviarValvulas, intervalValvulas);

//API REST
app.get('/ping', function(req, res) {
  var u = req.query.url
  if(!u.startsWith('http')) {
    u = 'http://' + u
  }
  request(u, function(error, response, body) {
    if(error) {
      res.json({msg: 'error'})
    }else{
      res.json({msg: 'ok'})
    }
  })
})

app.get('/url', function(req, res) {
  url = req.query.url
  if(!url.startsWith('http')) {
    url = 'http://' + url
  }
  request(url, function(error, response, body) {
    if(error) {
      res.json({msg: 'error'})
    }else{
      res.json({msg: 'ok'})
    }
  })
})

app.get('/activar/sensores', function(req, res) {
  sensores = true
  controlador1[0] = true
  controlador1[1] = true
  controlador1[2] = true
  controlador2[0] = true
  controlador2[1] = true
  controlador2[2] = true
  try {
    clearInterval(botSensores)
  }catch (e) {}
  botSensores = setInterval(enviarSensores, intervalSensores)
  res.json({msg: 'ok'})
})

app.get('/desactivar/sensores', function(req, res) {
  sensores = false
  controlador1[0] = false
  controlador1[1] = false
  controlador1[2] = false
  controlador2[0] = false
  controlador2[1] = false
  controlador2[2] = false
  try {
    clearInterval(botSensores)
  }catch (e) {}
  res.json({msg: 'ok'})
})

app.get('/intervalo/sensores/:value', function(req, res) {
  intervalSensores = req.params.value
  try {
    clearInterval(botSensores)
  }catch (e) {}
  if(sensores) {
    botSensores = setInterval(enviarSensores, intervalSensores)
  }
  res.json({msg: 'ok'})
})

app.get('/activar/valvulas', function(req, res) {
  valvulas = true
  controlador3[0] = true
  controlador3[1] = true
  controlador3[2] = true
  try {
    clearInterval(botValvulas)
  }catch (e) {}
  botValvulas = setInterval(enviarValvulas, intervalValvulas)
  res.json({msg: 'ok'})
})

app.get('/desactivar/valvulas', function(req, res) {
  valvulas = false
  controlador3[0] = false
  controlador3[1] = false
  controlador3[2] = false
  try {
    clearInterval(botValvulas)
  }catch (e) {}
  res.json({msg: 'ok'})
})

app.get('/intervalo/valvulas/:value', function(req, res) {
  intervalValvulas = req.params.value
  try {
    clearInterval(botValvulas)
  }catch (e) {}
  botValvulas = setInterval(enviarValvulas, intervalValvulas)
  res.json({msg: 'ok'})
})

app.get('/activar/:dispositivo', function(req, res) {
  var dispositivo = req.params.dispositivo
  switch (dispositivo) {
    case 'switch11':
      controlador1[0] = true
      break;
    case 'switch12':
      controlador1[1] = true
      break;
    case 'switch13':
      controlador1[2] = true
      break;
    case 'switch21':
      controlador2[0] = true
      break;
    case 'switch22':
      controlador2[1] = true
      break;
    case 'switch23':
      controlador2[2] = true
      break;
    case 'switch31':
      controlador3[0] = true
      break;
    case 'switch32':
      controlador3[1] = true
      break;
    case 'switch33':
      controlador3[2] = true
      break;
  }
  res.json({msg: 'ok'})
})

app.get('/desactivar/:dispositivo', function(req, res) {
  var dispositivo = req.params.dispositivo
  switch (dispositivo) {
    case 'switch11':
      controlador1[0] = false
      break;
    case 'switch12':
      controlador1[1] = false
      break;
    case 'switch13':
      controlador1[2] = false
      break;
    case 'switch21':
      controlador2[0] = false
      break;
    case 'switch22':
      controlador2[1] = false
      break;
    case 'switch23':
      controlador2[2] = false
      break;
    case 'switch31':
      controlador3[0] = false
      break;
    case 'switch32':
      controlador3[1] = false
      break;
    case 'switch33':
      controlador3[2] = false
      break;
  }
  res.json({msg: 'ok'})
})

app.get('/*', function(req, res) {
  res.redirect('/')
})

//Server
http.listen(config.port, function() {
  console.log('Servidor montado *:' + config.port)
})
